package com.estiam.algo;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner clavier= new Scanner(System.in);
		int saisieUtilisateur;
		char tableau[]={'a','b','c','d','e','f','g','h'};
		int matrice[][];
		int tailleSpirale=5;
		Integer buffer;
		System.out.println(tableau);
		try {
			Algos.rotationNElements(tableau, 1);
			System.out.println(tableau);
		} catch (OutOfBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		matrice=Algos.matriceSpirale(tailleSpirale);
		
		//Affichage matrice 
		for(int i=0;i<tailleSpirale;i++){
			for(int j=0;j<tailleSpirale;j++){
				buffer=matrice[i][j];
				System.out.print(buffer);
				if(buffer.toString().length()==1) System.out.print("  ");
				else System.out.print(" ");
			}
			System.out.println();
		}
		
		//Shake Hands
		
		System.out.println("Veuillez saisir le nombre de serrage de main :");
		saisieUtilisateur=clavier.nextInt();
		System.out.println(Algos.shakeHand(saisieUtilisateur)+" personnes étaient présentes");
		
	}

}
