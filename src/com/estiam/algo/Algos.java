package com.estiam.algo;

public class Algos {

	public static void rotationNElements(int tab[],int nRotation)throws OutOfBoundException{
		if (nRotation>tab.length){
			throw new OutOfBoundException();
		}
		else{
			int[] buffer = new int[nRotation];
			for(int i=0;i<nRotation;i++){
				buffer[i]=tab[i];
			}
			for(int i=0;i+nRotation<tab.length-1;i++){
				tab[i]=tab[i+nRotation];
			}
			for(int i=tab.length-nRotation;i<tab.length-1;i++){
				tab[i]=buffer[i-nRotation];
			}
				
			
		}
	}
	
	public static void rotationNElements(char tab[],int nRotation)throws OutOfBoundException{
		int depart;
		if (nRotation>tab.length){
			throw new OutOfBoundException();
		}
		else{
			char[] buffer = new char[nRotation];
			for(int i=0;i<nRotation;i++){
				buffer[i]=tab[i];
				
			}
			for(int i=0;i+nRotation<tab.length;i++){
				tab[i]=tab[i+nRotation];
			}
			depart=tab.length-nRotation;
			for(int i=tab.length-nRotation;i<tab.length;i++){
				tab[i]=buffer[i-depart];
			}						
		}
	}
	
	public static int[][] matriceSpirale(int taille){
		
		int matrice[][] = new int[taille][taille];
		int compteur1 = 2;
		int compteur = 0;
		int val=0;
		int num=0;
		for(val=0;val<3;val++){
			for(int j=0+val;j<taille-val;j++){
				matrice[0+val][j]=++num;
			}
			for(int i=1+val;i<taille-val;i++){
				matrice[i][taille-1-val]=++num;
			}
			for(int j=taille-2-val;j>=0+val;j--){
				matrice[taille-1-val][j]=++num;
			}
			for(int i=taille-2-val;i>0+val;i--){
				matrice[i][val]=++num;
			}
		}
		return matrice;
	}
	
	public static int shakeHand(int nbShake){
		int nbPerson;
			nbPerson=(int) Math.sqrt(2*nbShake);
		return nbPerson;
	}
}
